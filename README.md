Midnight Commander with scrollbar, mountpoints menu and F3 shows dirsize
======

![mc](https://dev.ussr.win/zlax/mc/raw/branch/master/mc.png)

**patches:**

1-scrollbar.patch - Scrollbar in active panel

2-f3-dirsize.patch - F3 shows dirsize

3-selmnt.patch - F11/F12 (Shift+F1/F2) Mountpoints menu

Authors: Oleg «Olegarch» Konovalov, [ose](https://www.linux.org.ru/people/ose/profile)

**files:**

mc-4.8.19-patches_ose.tbz - [mc patches pack by ose](https://www.linux.org.ru/forum/general/13373610?cid=13374027)

mc-4.8.22-debian10-patches.tar.gz - mc patches for mc-4.8.22 (debian 10)

mc-4.8.26-patches.tar.gz - mc patches for mc-4.8.26 (debian 11&12)

mc_4.8.26-1.1_amd64.deb, mc_4.8.26-1.1_arm64.deb, mc_4.8.26-1.1_armhf.deb, mc-data_4.8.26-1.1_all.deb - mc debian packages for amd64, arm64 and armhf

config_samples/ - config sample for advanced Mountpoints menu

### Installation:

**1. Install build dependencies:**
debian based:
```
sudo apt-get build-dep mc
```
redhat based:
```
sudo dnf builddep mc
```
**2. Configure:**
```
./autogen.sh
./configure
```
**3. Compile and install:**
```
make
sudo make install
```
